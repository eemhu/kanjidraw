﻿namespace KanjiDrawWFA
{
    /// <summary>
    /// Utilities used to manage the reading and writing the user's settings
    /// </summary>
    public static class SettingsUtilities
    {
        #region Reading and writing settings from resources
        /// <summary>
        /// Write settings from runtime settings to file using .NET Settings
        /// </summary>
        public static void WriteSettingsToFile(RuntimeSettings rtSettings)
        {
            Properties.Settings.Default.strokeSize = new [] { rtSettings.strokeWidth, rtSettings.strokeHeight };

            Properties.Settings.Default.penTipShape = rtSettings.penTip;
            Properties.Settings.Default.enableInkSmoothing = rtSettings.enableSmoothing;
            Properties.Settings.Default.enableEraseByStroke = rtSettings.enableEraseByStroke;

            Properties.Settings.Default.enablePressureSensitivity = rtSettings.enablePressureSensitivity;

            Properties.Settings.Default.enableLockCanvasToSquare = rtSettings.enableLockCanvasToSquare;

            Properties.Settings.Default.isFirstTimeBootUp = rtSettings.isFirstTimeBootUp;

            Properties.Settings.Default.enableGrid = rtSettings.enableGrid;
            Properties.Settings.Default.gridStyle = rtSettings.gridStyle;
            Properties.Settings.Default.gridMultis = rtSettings.gridMultis;

            Properties.Settings.Default.inkColor = rtSettings.inkColor;

            Properties.Settings.Default.canvasBgColor = rtSettings.canvasBgColor;

            Properties.Settings.Default.appBgColor = rtSettings.appBgColor;

            Properties.Settings.Default.appFgColor = rtSettings.appFgColor;

            Properties.Settings.Default.windowSize = rtSettings.windowSize;

            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Load settings from .NET Settings Resources to runtime settings object
        /// </summary>
        public static void LoadSettingsFromResources(ref RuntimeSettings rtSettings)
        {
            // rtSettings: stroke style
            rtSettings.strokeWidth = Properties.Settings.Default.strokeSize[0];
            rtSettings.strokeHeight = Properties.Settings.Default.strokeSize[1];
            rtSettings.penTip = Properties.Settings.Default.penTipShape;
            // rtSettings: smoothing
            rtSettings.enableSmoothing = Properties.Settings.Default.enableInkSmoothing;
            // rtSettings: erase by stroke or pixel-by-pixel
            rtSettings.enableEraseByStroke = Properties.Settings.Default.enableEraseByStroke;
            // rtSettings: Drawing tablet pressure sensitivity
            rtSettings.enablePressureSensitivity = Properties.Settings.Default.enablePressureSensitivity;
            // rtSettings: Lock canvas to 1:1
            rtSettings.enableLockCanvasToSquare = Properties.Settings.Default.enableLockCanvasToSquare;
            // rtSettings: First time bootup message
            rtSettings.isFirstTimeBootUp = Properties.Settings.Default.isFirstTimeBootUp;
            // rtSettings: Grid settings
            rtSettings.enableGrid = Properties.Settings.Default.enableGrid;
            rtSettings.gridStyle = Properties.Settings.Default.gridStyle;
            rtSettings.gridMultis = Properties.Settings.Default.gridMultis;
            // rtSettings: Color settings
            rtSettings.inkColor = Properties.Settings.Default.inkColor;
            rtSettings.canvasBgColor = Properties.Settings.Default.canvasBgColor;

            rtSettings.appBgColor = Properties.Settings.Default.appBgColor;
            rtSettings.appFgColor = Properties.Settings.Default.appFgColor;

            // rtSettings: Window size
            rtSettings.windowSize = Properties.Settings.Default.windowSize;
        }
        #endregion
    }
}

﻿namespace KanjiDrawWFA
{
    /// <summary>
    /// Wrapper class for the RuntimeSettings struct
    /// </summary>
    public class StructWrapper
    {
        public RuntimeSettings rts;
        public StructWrapper(ref RuntimeSettings rts)
        {
            this.rts = rts;
        }

    }
}

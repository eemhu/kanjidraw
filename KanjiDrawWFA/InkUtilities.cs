﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Ink;

namespace KanjiDrawWFA
{
    /// <summary>
    /// Utilities used to manipulate the ink on the canvas
    /// </summary>
    public static class InkUtilities
    {
        /// <summary>
        /// Sets the drawing attributes for the WPF InkCanvas object
        /// </summary>
        /// <param name="rgb">The color in [r, g, b] format (0-255)</param>
        public static void SetAttributes(InkCanvas c, RuntimeSettings rtSettings)
        {
            int[] rgb = rtSettings.inkColor;

            // Check if the rgb values are in range, and if they're not, throw an exception
            // this should never happen in production
            if (rgb[0] > 255 || rgb[1] > 255 || rgb[2] > 255 || rgb[0] < 0 || rgb[1] < 0 || rgb[2] < 0)
            {
                throw new Exception("Invalid rgb value of over 255 or less than 0, please contact the dev and provide info on when this error happened.");
            }

            // Set drawing attributes based on user settings
            DrawingAttributes da = new DrawingAttributes
            {
                StylusTip = rtSettings.penTip == 0 ? StylusTip.Ellipse : StylusTip.Rectangle,
                FitToCurve = rtSettings.enableSmoothing,
                Width = rtSettings.strokeWidth,
                Height = rtSettings.strokeHeight,
                Color = System.Windows.Media.Color.FromRgb((byte)rgb[0], (byte)rgb[1], (byte)rgb[2]),
                IgnorePressure = !rtSettings.enablePressureSensitivity
            };

            // Set the drawing attributes as the default
            c.DefaultDrawingAttributes = da;

            // Apply the new drawing attributes to the already existing strokes
            foreach (Stroke s in c.Strokes)
            {
                s.DrawingAttributes = da;
            }
        }

        /// <summary>
        /// Toggles the input mode between Pen and Eraser
        /// </summary>
        public static void ChangeInputMode(InkCanvas c, RuntimeSettings rtSettings, System.Windows.Forms.Button btnMode)
        {
            if (c.EditingMode == InkCanvasEditingMode.Ink)
            {
                btnMode.Text = "Pen";
                if (rtSettings.enableEraseByStroke)
                {
                    c.EditingMode = InkCanvasEditingMode.EraseByStroke;
                }
                else
                {
                    c.EditingMode = InkCanvasEditingMode.EraseByPoint;
                }
            }
            else
            {
                btnMode.Text = "Eraser";
                c.EditingMode = InkCanvasEditingMode.Ink;
            }
        }

        /// <summary>
        /// Clears all current ink on canvas
        /// </summary>
        public static void ClearAllInk(InkCanvas c, ToolStripStatusLabel statusLabel)
        {
            c.Strokes.Clear();
            statusLabel.Text = c.Strokes.Count + " stroke(s)";
        }

        /// <summary>
        /// Undoes the last stroke, and also saves it to REMOVED_STROKES for redo
        /// </summary>
        public static void UndoLastStroke(InkCanvas c, List<Stroke> removedStrokes, ToolStripStatusLabel statusLabel)
        {
            if (c.Strokes.Count > 0)
            {
                removedStrokes.Add(c.Strokes[c.Strokes.Count - 1]);
                c.Strokes.RemoveAt(c.Strokes.Count - 1);
            }

            if (removedStrokes.Count > 20)
            {
                removedStrokes.RemoveAt(0);
            }

            statusLabel.Text = c.Strokes.Count + " stroke(s)";

        }

        /// <summary>
        /// Redo previous stroke by returning it to the canvas from REMOVED_STROKES
        /// </summary>
        public static void RedoPreviousStroke(InkCanvas c, List<Stroke> removedStrokes, ToolStripStatusLabel statusLabel)
        {
            if (removedStrokes.Count > 0)
            {
                c.Strokes.Add(removedStrokes.ElementAt(removedStrokes.Count - 1));
                removedStrokes.RemoveAt(removedStrokes.Count - 1);
            }

            statusLabel.Text = c.Strokes.Count + " stroke(s)";
        }
    }
}

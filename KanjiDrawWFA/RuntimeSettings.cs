﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KanjiDrawWFA
{
    /// <summary>
    /// Contains the settings that are used in runtime.
    /// </summary>
    public struct RuntimeSettings
    {
        public int strokeHeight;
        public int strokeWidth;
        public int penTip;
        public bool enableSmoothing;
        public bool enableEraseByStroke;
        public bool enablePressureSensitivity;
        public bool enableLockCanvasToSquare;
        public bool isFirstTimeBootUp;
        public bool enableGrid;
        public double[] gridMultis;
        public int gridStyle;
        public int[] inkColor;
        public int[] canvasBgColor;
        public int[] appBgColor;
        public int[] appFgColor;
        public int[] windowSize;
        // custom bg?
    }
}

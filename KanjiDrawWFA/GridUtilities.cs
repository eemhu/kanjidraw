﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace KanjiDrawWFA
{
    /// <summary>
    /// Class containing the necessary functions for drawing a grid
    /// based on the user's settings
    /// </summary>
    public static class GridUtilities
    {
        #region Grid drawing functions

        /// <summary>
        /// Draw grid to the InkCanvas based on given offsets
        /// </summary>
        /// <param name="xOffset">Offset of x values: screen height divided by this value will be the number of rows</param>
        /// <param name="yOffset">Offset of y values: screen width divided by this value will be the number of columns</param>
        /// <param name="c">Target InkCanvas</param>
        public static void DrawGrid(int xOffset, int yOffset, InkCanvas c, RuntimeSettings rtSettings)
        {
            // Initialize lines object and set it to be beneath the canvas (z index)
            var lines = new System.Windows.Controls.Image();
            lines.SetValue(System.Windows.Controls.Panel.ZIndexProperty, -100);

            DrawingVisual gridLinesVisual = new DrawingVisual();
            DrawingContext dct = gridLinesVisual.RenderOpen();

            // "Pen" used to draw the lines
            var pen = new System.Windows.Media.Pen();

            // Set style of "pen" from runtime settings
            if (rtSettings.gridStyle == 0)
            {
                pen = new System.Windows.Media.Pen(System.Windows.Media.Brushes.Gray, 0.5f);
            }
            else if (rtSettings.gridStyle == 1)
            {
                pen = new System.Windows.Media.Pen(System.Windows.Media.Brushes.Black, 0.5f);
            }
            else if (rtSettings.gridStyle == 2)
            {
                pen = new System.Windows.Media.Pen(System.Windows.Media.Brushes.White, 0.5f);
            }
            else if (rtSettings.gridStyle == 3)
            {
                pen = new System.Windows.Media.Pen(System.Windows.Media.Brushes.Red, 0.5f);
            }

            // Calculate rows and cols
            int rows = (int)(SystemParameters.PrimaryScreenHeight) / yOffset;
            int cols = (int)(SystemParameters.PrimaryScreenWidth) / xOffset;
            int j = 0;


            // Horizontal lines
            var x = new System.Windows.Point(0, 0);
            var y = new System.Windows.Point(SystemParameters.PrimaryScreenWidth, 0);

            for (int i = 0; i <= rows; i++, j++)
            {
                dct.DrawLine(pen, x, y);
                x.Offset(0, yOffset);
                y.Offset(0, yOffset);
            }
            j = 0;

            // Vertical lines
            x = new System.Windows.Point(0, 0);
            y = new System.Windows.Point(0, SystemParameters.PrimaryScreenHeight);

            for (int i = 0; i <= cols; i++, j++)
            {
                dct.DrawLine(pen, x, y);
                x.Offset(xOffset, 0);
                y.Offset(xOffset, 0);
            }

            // Close render
            dct.Close();

            // Create bitmap and set it as a child of the target inkcanvas
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)SystemParameters.PrimaryScreenWidth, (int)SystemParameters.PrimaryScreenHeight, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(gridLinesVisual);
            bmp.Freeze();
            lines.Source = bmp;

            c.Children.Add(lines);
        }

        /// <summary>
        /// Remove grid from the target InkCanvas
        /// Goes through all children of c and removes the first image (grid is a bitmap)
        /// </summary>
        /// <param name="c">Target InkCanvas</param>
        public static void RemoveGrid(InkCanvas c)
        {
            foreach (UIElement obj in c.Children)
            {
                if (obj is System.Windows.Controls.Image)
                {
                    c.Children.Remove(obj);
                    break;
                }
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace KanjiDrawWFA
{
    public partial class Form1 : Form
    {
        #region Constructors of main form
        public Form1()
        {
            InitializeComponent();
        }
        #endregion 

        #region Initialization of global variables
        
        // Program settings are stored on this global variable (struct)
        private RuntimeSettings _runtimeSettings;
        
        // The main WPF Ink Canvas object, where the user draws
        private InkCanvas _inkCanvas;

        // List of strokes used for undo/redo functionality
        private readonly List<Stroke> _removedStrokes = new List<Stroke>();

        // Current window state, meaning "minimized", "maximized" or "normal"
        private FormWindowState _currentWindowState;

        // List used to store previous drawings, used for the "Rescue Previous Drawings" option
        private readonly List<StrokeCollection> _previousDrawings = new List<StrokeCollection>();

        // custom background variables (if the user has custom_bg.png in the same folder as the executable
        private bool _customBackgroundEnabled;
        private System.Drawing.Image _customBackground;
        #endregion

        /// <summary>
        /// Updates the form style from runtime settings
        /// Allows the potential for user editable interface colors
        /// </summary>
        private void UpdateFormStyle()
        {
            int bgColorR = _runtimeSettings.appBgColor[0];   int fgColorR = _runtimeSettings.appFgColor[0];
            int bgColorG = _runtimeSettings.appBgColor[1];   int fgColorG = _runtimeSettings.appFgColor[1];
            int bgColorB = _runtimeSettings.appBgColor[2];   int fgColorB = _runtimeSettings.appFgColor[2];

            this.BackColor = System.Drawing.Color.FromArgb(bgColorR, bgColorG, bgColorB);
            this.ForeColor = System.Drawing.Color.FromArgb(fgColorR, fgColorG, fgColorB);
            menuStrip1.BackColor = System.Drawing.Color.FromArgb(bgColorR, bgColorG, bgColorB);
            menuStrip1.ForeColor = System.Drawing.Color.FromArgb(fgColorR, fgColorG, fgColorB);
            statusStrip1.BackColor = System.Drawing.Color.FromArgb(bgColorR, bgColorG, bgColorB);
            statusStrip1.ForeColor = System.Drawing.Color.FromArgb(fgColorR, fgColorG, fgColorB);
            btnClear.BackColor = System.Drawing.Color.FromArgb(bgColorR, bgColorG, bgColorB);
            btnClear.ForeColor = System.Drawing.Color.FromArgb(fgColorR, fgColorG, fgColorB);
            btnMode.BackColor = System.Drawing.Color.FromArgb(bgColorR, bgColorG, bgColorB);
            btnMode.ForeColor = System.Drawing.Color.FromArgb(fgColorR, fgColorG, fgColorB);
            btnColorSwap.BackColor = System.Drawing.Color.FromArgb(bgColorR, bgColorG, bgColorB);
            btnColorSwap.ForeColor = System.Drawing.Color.FromArgb(fgColorR, fgColorG, fgColorB);

            btnChangeBGColor.ForeColor = System.Drawing.Color.FromArgb(fgColorR, fgColorG, fgColorB);
            btnChangeInkColor.ForeColor = System.Drawing.Color.FromArgb(fgColorR, fgColorG, fgColorB);
        }

        /// <summary>
        /// Used to apply user's settings to GUI when they're changed or read for the first time
        /// </summary>
        private void ApplySettingsChangesToGui(Form mainForm)
        {
            // Apply changes to GUI
            mainForm.Size = new System.Drawing.Size(_runtimeSettings.windowSize[0], _runtimeSettings.windowSize[1]);

            _inkCanvas.Background = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, (byte)_runtimeSettings.canvasBgColor[0], (byte)_runtimeSettings.canvasBgColor[1], (byte)_runtimeSettings.canvasBgColor[2]));
            InkUtilities.SetAttributes(_inkCanvas, _runtimeSettings);

            inkSmoothingToolStripMenuItem.CheckState = _runtimeSettings.enableSmoothing ? CheckState.Checked : CheckState.Unchecked;
            eraseByStrokeToolStripMenuItem.CheckState = _runtimeSettings.enableEraseByStroke ? CheckState.Checked : CheckState.Unchecked;
            // gridToolStripMenuItem.CheckState = rtSettings.enableGrid ? CheckState.Checked : CheckState.Unchecked;

            // Grid style GUI change
            if (_runtimeSettings.enableGrid)
            {
                defaultToolStripMenuItem.Checked = _runtimeSettings.gridStyle == 0;
                blackToolStripMenuItem.Checked = _runtimeSettings.gridStyle == 1;
                whiteToolStripMenuItem.Checked = _runtimeSettings.gridStyle == 2;
                redToolStripMenuItem.Checked = _runtimeSettings.gridStyle == 3;
            }
        }

        /// <summary>
        /// Startup sequence
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            // Update title
            this.Text = Resources.Strings.KANJIDRAW_WITH_V + Assembly.GetExecutingAssembly().GetName().Version;
            // Reset settings of app if SHIFT is being held down on start
            bool resetSettings = Keyboard.Modifiers.HasFlag(System.Windows.Input.ModifierKeys.Shift);
            
            // Initialize ink canvas and set its host (WPF ElementHost)
            _inkCanvas = new InkCanvas();
            mainHost.Child = _inkCanvas;

            // Handle settings upgrade if app gets updated
            if (!resetSettings && Properties.Settings.Default.isAppUpdated)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.isAppUpdated = false;
            }
            // Settings reset because of user held SHIFT down on boot
            else if (resetSettings)
            {
                System.Windows.MessageBox.Show(Resources.Strings.DIALOG_CONTENT_SETTINGS_RESET,
                    Resources.Strings.DIALOG_TITLE_SETTINGS_RESET, MessageBoxButton.OK, MessageBoxImage.Information);
                Properties.Settings.Default.Reset();
            }

            // Load settings from Resources to runtime object
            SettingsUtilities.LoadSettingsFromResources(ref _runtimeSettings);

            // Apply UI changes
            ApplySettingsChangesToGui(this);

            // Set pen width/height NumericUpDowns values
            nudW.Value = _runtimeSettings.strokeWidth;
            nudH.Value = _runtimeSettings.strokeHeight;

            // Set methods to handle InkCanvas stroke collection and erase events
            _inkCanvas.StrokeCollected += INK_CANVAS_StrokeCollected;
            _inkCanvas.StrokeErased += INK_CANVAS_StrokeErased;
            
            // Add key down handler to ink canvas (used for stroke order diagram movement)
            _inkCanvas.KeyDown += Form_KeyDown;

            // Set background color of BG/FG color selection buttons to match the active color
            btnChangeBGColor.BackColor = System.Drawing.Color.FromArgb(255, _runtimeSettings.canvasBgColor[0], _runtimeSettings.canvasBgColor[1], _runtimeSettings.canvasBgColor[2]);
            btnChangeInkColor.BackColor = System.Drawing.Color.FromArgb(255, _runtimeSettings.inkColor[0], _runtimeSettings.inkColor[1], _runtimeSettings.inkColor[2]);

            // Update form style and menuStrip style from custom colors
            UpdateFormStyle();
            menuStrip1.Renderer = new ToolStripProfessionalRenderer(new CustomColorTable());

            // set custom background if it exists
            if (File.Exists(Resources.Values.PATH_CUSTOM_BG))
            {
                _customBackground = System.Drawing.Image.FromFile(Resources.Values.PATH_CUSTOM_BG);
                this.BackgroundImage = _customBackground;
                int w = this.BackgroundImage.Width;
                int h = this.BackgroundImage.Height;

                this.Size = new System.Drawing.Size(w, h);
                this.BackgroundImageLayout = ImageLayout.Stretch;
                mainHost.BackColor = System.Drawing.Color.Transparent;

                _inkCanvas.Background = new SolidColorBrush(System.Windows.Media.Color.FromArgb(200, 200, 200, 200));

                btnChangeBGColor.Visible = false;
                label3.Visible = false;
                btnColorSwap.Visible = false;

                _customBackgroundEnabled = true;
            }

            // Display first boot message, if necessary
            if (_runtimeSettings.isFirstTimeBootUp)
            {
                System.Windows.Forms.MessageBox.Show(
                    Resources.Strings.WELCOME_MESSAGE,
                    Resources.Strings.WELCOME_MESSAGE_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Information);
                _runtimeSettings.isFirstTimeBootUp = false;
            }

            _currentWindowState = this.WindowState;

            // fill the panel if not locking to square
            if (!_runtimeSettings.enableLockCanvasToSquare)
            {
                mainHost.Dock = DockStyle.Fill;
            }

            if (_runtimeSettings.enableGrid) GridUtilities.DrawGrid((int)Math.Ceiling(_inkCanvas.ActualWidth * _runtimeSettings.gridMultis[0]), (int)Math.Ceiling(_inkCanvas.ActualHeight * _runtimeSettings.gridMultis[1]), _inkCanvas, _runtimeSettings); // lines

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SettingsUtilities.WriteSettingsToFile(_runtimeSettings);
        }

        /// <summary>
        /// On KeyDown, check if the pressed down keys match to hotkeys and
        /// execute the given method if a match is found
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            // CTRL+S - Clear all ink
            if (e.Key == Key.S && (Keyboard.Modifiers & System.Windows.Input.ModifierKeys.Control) == System.Windows.Input.ModifierKeys.Control)
            {
                if (_inkCanvas.Strokes.Count > 0) 
                {
                    _previousDrawings.Add(_inkCanvas.Strokes.Clone()); // PD

                    InkUtilities.ClearAllInk(_inkCanvas, statusLabel);
                }
                e.Handled = true;
            }
            // CTRL+Z - Undo
            else if (e.Key == Key.Z && (Keyboard.Modifiers & System.Windows.Input.ModifierKeys.Control) == System.Windows.Input.ModifierKeys.Control)
            {
                InkUtilities.UndoLastStroke(_inkCanvas, _removedStrokes, statusLabel);
                e.Handled = true;
            }
            // CTRL+Y - Redo
            else if (e.Key == Key.Y && (Keyboard.Modifiers & System.Windows.Input.ModifierKeys.Control) == System.Windows.Input.ModifierKeys.Control)
            {
                InkUtilities.RedoPreviousStroke(_inkCanvas, _removedStrokes, statusLabel);
                e.Handled = true;
            }
            // CTRL+E - Change Input Mode
            else if (e.Key == Key.E && (Keyboard.Modifiers & System.Windows.Input.ModifierKeys.Control) == System.Windows.Input.ModifierKeys.Control)
            {
                InkUtilities.ChangeInputMode(_inkCanvas, _runtimeSettings, btnMode);
                e.Handled = true;
            }
            // CTRL+W - Move stroke order diagram
            else if (e.Key == Key.W && (Keyboard.Modifiers & System.Windows.Input.ModifierKeys.Control) == System.Windows.Input.ModifierKeys.Control)
            {
                StrokeOrderDiagram.DrawStrokeOrder(_inkCanvas, toolStripStrokeOrderTextBox.Text);
                e.Handled = true;
            }
        }

        private void INK_CANVAS_StrokeErased(object sender, RoutedEventArgs e)
        {
            statusLabel.Text = _inkCanvas.Strokes.Count + Resources.Strings.STROKE_AMOUNT_SUFFIX;
        }

        private void INK_CANVAS_StrokeCollected(object sender, InkCanvasStrokeCollectedEventArgs e)
        {
            statusLabel.Text = _inkCanvas.Strokes.Count + Resources.Strings.STROKE_AMOUNT_SUFFIX;
        }

        /// <summary>
        /// Change pen ink width
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NudW_ValueChanged(object sender, EventArgs e)
        {
            _runtimeSettings.strokeWidth = (int)nudW.Value;
            InkUtilities.SetAttributes(_inkCanvas, _runtimeSettings);
        }

        /// <summary>
        /// Change pen ink height
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NudH_ValueChanged(object sender, EventArgs e)
        {
            _runtimeSettings.strokeHeight = (int)nudH.Value;
            InkUtilities.SetAttributes(_inkCanvas, _runtimeSettings);
        }

        #region Buttons
        private void BtnMode_Click(object sender, EventArgs e)
        {
            InkUtilities.ChangeInputMode(_inkCanvas, _runtimeSettings, btnMode);
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            if (_inkCanvas.Strokes.Count > 0)
            {
                _previousDrawings.Add(_inkCanvas.Strokes.Clone()); // PD
                
                InkUtilities.ClearAllInk(_inkCanvas, statusLabel);

                if (_previousDrawings.Count > 5)
                {
                    _previousDrawings.RemoveAt(0);
                }
            }
        }

        private void BtnChangeInkColor_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();

            DialogResult res = cd.ShowDialog();

            if (res == DialogResult.OK)
            {
                System.Drawing.Color c = cd.Color;
                _runtimeSettings.inkColor[0] = c.R;
                _runtimeSettings.inkColor[1] = c.G;
                _runtimeSettings.inkColor[2] = c.B;
                InkUtilities.SetAttributes(_inkCanvas, _runtimeSettings);
                btnChangeInkColor.BackColor = System.Drawing.Color.FromArgb(255, _runtimeSettings.inkColor[0], _runtimeSettings.inkColor[1], _runtimeSettings.inkColor[2]);
            }
        }

        private void BtnChangeBGColor_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();

            DialogResult res = cd.ShowDialog();

            if (res == DialogResult.OK)
            {
                System.Drawing.Color c = cd.Color;
                _runtimeSettings.canvasBgColor[0] = c.R;
                _runtimeSettings.canvasBgColor[1] = c.G;
                _runtimeSettings.canvasBgColor[2] = c.B;
                _inkCanvas.Background = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, (byte)_runtimeSettings.canvasBgColor[0], (byte)_runtimeSettings.canvasBgColor[1], (byte)_runtimeSettings.canvasBgColor[2]));
                btnChangeBGColor.BackColor = System.Drawing.Color.FromArgb(255, _runtimeSettings.canvasBgColor[0], _runtimeSettings.canvasBgColor[1], _runtimeSettings.canvasBgColor[2]);
            }
        }

        private void BtnColorSwap_Click(object sender, EventArgs e)
        {
            byte tempRed = (byte)_runtimeSettings.inkColor[0];
            byte tempGreen = (byte)_runtimeSettings.inkColor[1];
            byte tempBlue = (byte)_runtimeSettings.inkColor[2];

            _runtimeSettings.inkColor[0] = _runtimeSettings.canvasBgColor[0];
            _runtimeSettings.inkColor[1] = _runtimeSettings.canvasBgColor[1];
            _runtimeSettings.inkColor[2] = _runtimeSettings.canvasBgColor[2];

            _runtimeSettings.canvasBgColor[0] = tempRed;
            _runtimeSettings.canvasBgColor[1] = tempGreen;
            _runtimeSettings.canvasBgColor[2] = tempBlue;

            btnChangeBGColor.BackColor = System.Drawing.Color.FromArgb(
                255,
                _runtimeSettings.canvasBgColor[0],
                _runtimeSettings.canvasBgColor[1],
                _runtimeSettings.canvasBgColor[2]
                );
            
            _inkCanvas.Background = new SolidColorBrush(
                System.Windows.Media.Color.FromArgb(
                    255,
                    (byte)_runtimeSettings.canvasBgColor[0],
                    (byte)_runtimeSettings.canvasBgColor[1],
                    (byte)_runtimeSettings.canvasBgColor[2]
                    )
                );

            btnChangeInkColor.BackColor = System.Drawing.Color.FromArgb(
                255,
                _runtimeSettings.inkColor[0],
                _runtimeSettings.inkColor[1],
                _runtimeSettings.inkColor[2]
                );
            
            InkUtilities.SetAttributes(_inkCanvas, _runtimeSettings);
        }
        #endregion

        #region Handle form window resizing (grid and square canvas)

        private System.Drawing.Size _currentSize;
        private readonly System.Drawing.Size _zeroSize = System.Drawing.Size.Empty;
        private bool _currentResizeGridRemoved;
        /// <summary>
        /// On form resize end, redraw grid if necessary 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            
            if (_runtimeSettings.enableGrid)
            {
                GridUtilities.RemoveGrid(_inkCanvas);
                GridUtilities.DrawGrid((int)Math.Ceiling(_inkCanvas.ActualWidth * _runtimeSettings.gridMultis[0]), (int)Math.Ceiling(_inkCanvas.ActualHeight * _runtimeSettings.gridMultis[1]), _inkCanvas, _runtimeSettings);
            }

            if (_customBackgroundEnabled) this.BackgroundImage = _customBackground;
        }

        /// <summary>
        /// On form resize begin, set current size into a variable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_ResizeBegin(object sender, EventArgs e)
        {
            _currentSize = this.Size;
            _currentResizeGridRemoved = false;
            
            if (_customBackgroundEnabled) this.BackgroundImage = null;
        }

        /// <summary>
        /// On resize, remove grid from canvas if necessary and the form has been actually resized
        /// -> "Resize" event occurs even without 'actual' resizing
        /// -> That's why we need to check if the size actually changes to avoid unnecessary grid redraws
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Resize(object sender, EventArgs e)
        {
            var diff = _currentSize - this.Size;
            if (!_currentResizeGridRemoved && _runtimeSettings.enableGrid && diff != _zeroSize)
            {
                GridUtilities.RemoveGrid(_inkCanvas);
                _currentResizeGridRemoved = true;
            }
        }

        /// <summary>
        /// Redraw grid and make sure the canvas is a square after form window resize
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            System.Drawing.Size formSize = this.Size;

            _runtimeSettings.windowSize[0] = formSize.Width;
            _runtimeSettings.windowSize[1] = formSize.Height;

            // make sure the canvas is a square when resized
            if (_runtimeSettings.enableLockCanvasToSquare)
            {
                int smallest = Math.Min(mainPanel.Width, mainPanel.Height);
                mainHost.Size = new System.Drawing.Size(smallest, smallest);
                int diffWidth = Math.Abs(mainPanel.Width - smallest);
                int diffHeight = Math.Abs(mainPanel.Height - smallest);
                mainHost.Location = new System.Drawing.Point(diffWidth / 2, diffHeight / 2);
            }


            if (_currentWindowState != this.WindowState)
            {
                _currentWindowState = this.WindowState;
                if (_runtimeSettings.enableGrid)
                {
                    GridUtilities.RemoveGrid(_inkCanvas);
                    GridUtilities.DrawGrid(
                        (int)Math.Ceiling(_inkCanvas.ActualWidth * _runtimeSettings.gridMultis[0]), 
                        (int)Math.Ceiling(_inkCanvas.ActualHeight * _runtimeSettings.gridMultis[1]),
                        _inkCanvas,
                        _runtimeSettings
                        );
                }
            }
        }

        #endregion
       
        /// <summary>
        /// Opens the advanced settings menu and saves the settings into the runtime settings struct
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdvancedSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Wrap the runtime settings in a wrapper class - required to cause the struct to update
            // after being passed to the advanced settings form
            StructWrapper rtsWrapped = new StructWrapper(ref _runtimeSettings);

            // Show advanced config form and update runtime settings after
            new advancedConfigForm(ref rtsWrapped).ShowDialog();
            _runtimeSettings = rtsWrapped.rts;

            // Update all the necessary variables due to settings change
            if (!_runtimeSettings.enableLockCanvasToSquare)
            {
                mainHost.Dock = DockStyle.Fill;
                mainHost.Size = new System.Drawing.Size(mainPanel.Width, mainPanel.Height);
                mainHost.Location = new System.Drawing.Point(0, 0);
            }
            else
            {
                mainHost.Dock = DockStyle.None;
                int smallest = Math.Min(mainPanel.Width, mainPanel.Height);
                mainHost.Size = new System.Drawing.Size(smallest, smallest);
                int diffWidth = Math.Abs(mainPanel.Width - smallest);
                int diffHeight = Math.Abs(mainPanel.Height - smallest);
                mainHost.Location = new System.Drawing.Point(diffWidth/2, diffHeight/2);
            }

            if (_runtimeSettings.enableGrid)
            {
                GridUtilities.RemoveGrid(_inkCanvas);
                GridUtilities.DrawGrid(
                    (int)Math.Ceiling(_inkCanvas.ActualWidth * _runtimeSettings.gridMultis[0]), 
                    (int)Math.Ceiling(_inkCanvas.ActualHeight * _runtimeSettings.gridMultis[1]),
                    _inkCanvas,
                    _runtimeSettings
                    );
            }

            InkUtilities.SetAttributes(_inkCanvas, _runtimeSettings);
            UpdateFormStyle();
        }

        #region Misc toolstrip menu items
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingsUtilities.WriteSettingsToFile(_runtimeSettings);
            Environment.Exit(0);
        }

        private void CheckForUpdatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dr = System.Windows.Forms.MessageBox.Show(
                Resources.Strings.DIALOG_CONTENT_VISIT_UPDATE_SITE, Resources.Strings.DIALOG_TITLE_VISIT_UPDATE_SITE,
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dr == DialogResult.Yes)
            {
                System.Diagnostics.Process.Start(Resources.Values.URL_UPDATE_SITE);
            }
        }
        
        private void EraseByStrokeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _runtimeSettings.enableEraseByStroke = eraseByStrokeToolStripMenuItem.CheckState == CheckState.Checked;

            if (_inkCanvas.EditingMode != InkCanvasEditingMode.Ink)
            {
                _inkCanvas.EditingMode = _runtimeSettings.enableEraseByStroke ? 
                    InkCanvasEditingMode.EraseByStroke : InkCanvasEditingMode.EraseByPoint;
            }
        }
        private void InkSmoothingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _runtimeSettings.enableSmoothing = inkSmoothingToolStripMenuItem.CheckState == CheckState.Checked;
            InkUtilities.SetAttributes(_inkCanvas, _runtimeSettings);
        }
        #endregion

        #region Grid toolstrip menu items
        private void defaultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_runtimeSettings.enableGrid && (sender as ToolStripMenuItem).Checked)
            {
                _runtimeSettings.enableGrid = false;
                defaultToolStripMenuItem.Checked = false;
                blackToolStripMenuItem.Checked = false;
                whiteToolStripMenuItem.Checked = false;
                redToolStripMenuItem.Checked = false;
                GridUtilities.RemoveGrid(_inkCanvas);
            }
            else
            {
                _runtimeSettings.enableGrid = true;
                _runtimeSettings.gridStyle = 0;
                defaultToolStripMenuItem.Checked = true;
                blackToolStripMenuItem.Checked = false;
                whiteToolStripMenuItem.Checked = false;
                redToolStripMenuItem.Checked = false;
                GridUtilities.RemoveGrid(_inkCanvas);
                GridUtilities.DrawGrid((int)Math.Ceiling(_inkCanvas.ActualWidth * _runtimeSettings.gridMultis[0]), (int)Math.Ceiling(_inkCanvas.ActualHeight * _runtimeSettings.gridMultis[1]), _inkCanvas, _runtimeSettings);
            }
        }

        private void blackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_runtimeSettings.enableGrid && (sender as ToolStripMenuItem).Checked)
            {
                _runtimeSettings.enableGrid = false;
                defaultToolStripMenuItem.Checked = false;
                blackToolStripMenuItem.Checked = false;
                whiteToolStripMenuItem.Checked = false;
                redToolStripMenuItem.Checked = false;
                GridUtilities.RemoveGrid(_inkCanvas);
            }
            else
            {
                _runtimeSettings.enableGrid = true;
                _runtimeSettings.gridStyle = 1;
                defaultToolStripMenuItem.Checked = false;
                blackToolStripMenuItem.Checked = true;
                whiteToolStripMenuItem.Checked = false;
                redToolStripMenuItem.Checked = false;
                GridUtilities.RemoveGrid(_inkCanvas);
                GridUtilities.DrawGrid((int)Math.Ceiling(_inkCanvas.ActualWidth * _runtimeSettings.gridMultis[0]), (int)Math.Ceiling(_inkCanvas.ActualHeight * _runtimeSettings.gridMultis[1]), _inkCanvas, _runtimeSettings);
            }
        }

        private void whiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_runtimeSettings.enableGrid && (sender as ToolStripMenuItem).Checked)
            {
                _runtimeSettings.enableGrid = false;
                defaultToolStripMenuItem.Checked = false;
                blackToolStripMenuItem.Checked = false;
                whiteToolStripMenuItem.Checked = false;
                redToolStripMenuItem.Checked = false;
                GridUtilities.RemoveGrid(_inkCanvas);
            }
            else
            {
                _runtimeSettings.enableGrid = true;
                _runtimeSettings.gridStyle = 2;
                defaultToolStripMenuItem.Checked = false;
                blackToolStripMenuItem.Checked = false;
                whiteToolStripMenuItem.Checked = true;
                redToolStripMenuItem.Checked = false;
                GridUtilities.RemoveGrid(_inkCanvas);
                GridUtilities.DrawGrid((int)Math.Ceiling(_inkCanvas.ActualWidth * _runtimeSettings.gridMultis[0]), (int)Math.Ceiling(_inkCanvas.ActualHeight * _runtimeSettings.gridMultis[1]), _inkCanvas, _runtimeSettings);
            }
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_runtimeSettings.enableGrid && (sender as ToolStripMenuItem).Checked)
            {
                _runtimeSettings.enableGrid = false;
                defaultToolStripMenuItem.Checked = false;
                blackToolStripMenuItem.Checked = false;
                whiteToolStripMenuItem.Checked = false;
                redToolStripMenuItem.Checked = false;
                GridUtilities.RemoveGrid(_inkCanvas);
            }
            else
            {
                _runtimeSettings.enableGrid = true;
                _runtimeSettings.gridStyle = 3;
                defaultToolStripMenuItem.Checked = false;
                blackToolStripMenuItem.Checked = false;
                whiteToolStripMenuItem.Checked = false;
                redToolStripMenuItem.Checked = true;
                GridUtilities.RemoveGrid(_inkCanvas);
                GridUtilities.DrawGrid((int)Math.Ceiling(_inkCanvas.ActualWidth * _runtimeSettings.gridMultis[0]), (int)Math.Ceiling(_inkCanvas.ActualHeight * _runtimeSettings.gridMultis[1]), _inkCanvas, _runtimeSettings);
            }
        }
        #endregion
        
        #region undo and redo toolstrip menu items
        private void undoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            InkUtilities.UndoLastStroke(_inkCanvas, _removedStrokes, statusLabel);
        }

        private void redoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            InkUtilities.RedoPreviousStroke(_inkCanvas, _removedStrokes, statusLabel);
        }
        #endregion

        #region Rescue previous drawing toolstrip menu
        
        /// <summary>
        /// Create rescue previous drawing dropdown when hovering over
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RescuePreviousDrawingToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            // clear old items
            rescuePreviousDrawingToolStripMenuItem.DropDownItems.Clear();
            
            _previousDrawings.ForEach(collection =>
            {
                string index = (_previousDrawings.IndexOf(collection) + 1).ToString();
                // index is actually index + 1, call with index - 1
                var newItem = new ToolStripMenuItem
                {
                    Name = index,
                    Text = index
                };

                newItem.Click += RescuePreviousDrawing_DropDownItem_Click;
                rescuePreviousDrawingToolStripMenuItem.DropDownItems.Add(newItem);
            });
        }

        /// <summary>
        /// On rescue previous drawing dropdown item click, rescue drawing onto canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RescuePreviousDrawing_DropDownItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem i = sender as ToolStripMenuItem;

            int index = int.Parse(i.Text.ToString());

            _inkCanvas.Strokes = _previousDrawings[index - 1].Clone();
        }
        #endregion

        /// <summary>
        /// Draw stroke order diagram if user has changed the source string
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripStrokeOrderTextBox_TextChanged(object sender, EventArgs e)
        {
            StrokeOrderDiagram.DrawStrokeOrder(_inkCanvas, toolStripStrokeOrderTextBox.Text);
        }
    }
}

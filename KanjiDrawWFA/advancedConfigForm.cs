﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KanjiDrawWFA
{
    public partial class advancedConfigForm : Form
    {
        private StructWrapper rtsWrapper;

        public advancedConfigForm()
        {
            InitializeComponent();
        }

        public advancedConfigForm(ref StructWrapper rtsWrapper)
        {
            this.rtsWrapper = rtsWrapper;
            InitializeComponent();
        }

        /// <summary>
        /// On config form load, make sure UI matches the current settings values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdvancedConfigForm_Load(object sender, EventArgs e)
        {
            // Prepare version label: remove last two numbers (-> only major/minor version numbers)
            String versionLbl = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            int removedDots = 0;

            while (removedDots < 2)
            {
                versionLbl = versionLbl.Substring(0, versionLbl.LastIndexOf("."));
                removedDots++;
            }

            // Update version label
            lblKdTitle.Text = Resources.Strings.KANJIDRAW_WITH_V + versionLbl;
            
            nudGridSize.Value = Convert.ToDecimal(1d / rtsWrapper.rts.gridMultis[0]);
            
            if (rtsWrapper.rts.penTip == 0)
            {
                rbPenTip0.Checked = true;
                rbPenTip1.Checked = false;
            }
            else
            {
                rbPenTip0.Checked = false;
                rbPenTip1.Checked = true;
            }

            cbPressureSensitivity.Checked = rtsWrapper.rts.enablePressureSensitivity;
            cbLockCanvasToSquare.Checked = rtsWrapper.rts.enableLockCanvasToSquare;

            UpdateGridLabel();
        }

        /// <summary>
        /// Set struct values on save button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_Click(object sender, EventArgs e)
        {
            rtsWrapper.rts.gridMultis[0] = 1d / (double)nudGridSize.Value;
            rtsWrapper.rts.gridMultis[1] = 1d / (double)nudGridSize.Value;
            rtsWrapper.rts.penTip = rbPenTip0.Checked ? 0 : 1;
            rtsWrapper.rts.enablePressureSensitivity = cbPressureSensitivity.Checked;
            rtsWrapper.rts.enableLockCanvasToSquare = cbLockCanvasToSquare.Checked;

            this.Dispose();

        }

        /// <summary>
        /// Updates the label next to the numericupdown component
        /// to help the user recognize what kind of grid the value will result in
        /// </summary>
        private void UpdateGridLabel()
        {
            // dimension: g = 1/x
            // x = 1/g
            lblGridMultiplier.Text = string.Format(Resources.Strings.CONFIG_GRID_SIZE_EXPLANATION, nudGridSize.Value,
                nudGridSize.Value); 

            if (nudGridSize.Value == 2 || nudGridSize.Value == 4 || nudGridSize.Value % 5 == 0)
            {
                lblGridMultiplier.ForeColor = Color.Green;
            }
            else
            {
                lblGridMultiplier.ForeColor = Color.Red;
            }
        }

        // Update grid label on value change
        private void nudGridSize_ValueChanged(object sender, EventArgs e)
        {
            UpdateGridLabel();
        }

        // Open downloads page after confirmation
        private void lbCheckForUpdates_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var dr = MessageBox.Show(Resources.Strings.DIALOG_CONTENT_VISIT_UPDATE_SITE,
                Resources.Strings.DIALOG_TITLE_VISIT_UPDATE_SITE, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dr == DialogResult.Yes)
            {
                System.Diagnostics.Process.Start(Resources.Values.URL_UPDATE_SITE);
            }
        }
    }
}

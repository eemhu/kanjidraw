﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace KanjiDrawWFA
{
    /// <summary>
    /// Class containing all the functions and variables relating to drawing the
    /// stroke order diagram
    /// </summary>
    public static class StrokeOrderDiagram
    {
        public static Label strokeOrderLabel;
        /// <summary>
        /// Draws the stroke order diagram to target canvas c from source string src
        /// </summary>
        /// <param name="c">target inkcanvas</param>
        /// <param name="src">source string</param>
        public static void DrawStrokeOrder(InkCanvas c, string src)
        {
            // Remove any already existing stroke order diagrams
            c.Children.Remove(strokeOrderLabel);

            // Position of mouse on canvas
            Point p = Mouse.GetPosition(c);

            // Create label to be used as the diagram
            strokeOrderLabel = new Label
            {
                Content = src,
                FontFamily = new FontFamily("KanjiStrokeOrders"),
                FontSize = c.ActualWidth / 2,
                Foreground = new SolidColorBrush(Color.FromArgb(100, 255, 255, 255)),
                VerticalContentAlignment = VerticalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center
            };

            // Make the diagram appear below the ink
            strokeOrderLabel.SetValue(Panel.ZIndexProperty, -50);

            // Change sizing of diagram
            strokeOrderLabel.RenderTransform = new TranslateTransform
            {
                X = (p.X > 0 ? p.X : (c.ActualWidth / 2)) - strokeOrderLabel.FontSize / 2,
                Y = (p.Y > 0 ? p.Y : (c.ActualHeight / 2)) - strokeOrderLabel.FontSize * 0.75d
            };

            // Possible rotations can be handled here, if needed
            strokeOrderLabel.LayoutTransform = new RotateTransform
            {

            };

            // Add to canvas
            c.Children.Add(strokeOrderLabel);
        }
    }
}

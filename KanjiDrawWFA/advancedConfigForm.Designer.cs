﻿namespace KanjiDrawWFA
{
    partial class advancedConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(advancedConfigForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nudGridSize = new System.Windows.Forms.NumericUpDown();
            this.lblGridMultiplier = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbPenTip1 = new System.Windows.Forms.RadioButton();
            this.rbPenTip0 = new System.Windows.Forms.RadioButton();
            this.cbPressureSensitivity = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbLockCanvasToSquare = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pbTablet = new System.Windows.Forms.PictureBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.lbCheckForUpdates = new System.Windows.Forms.LinkLabel();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.lblSubTitle = new System.Windows.Forms.Label();
            this.lblKdTitle = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGridSize)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbTablet)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nudGridSize);
            this.groupBox1.Controls.Add(this.lblGridMultiplier);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(436, 56);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Grid layout";
            // 
            // nudGridSize
            // 
            this.nudGridSize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.nudGridSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudGridSize.ForeColor = System.Drawing.Color.White;
            this.nudGridSize.Location = new System.Drawing.Point(9, 18);
            this.nudGridSize.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
            this.nudGridSize.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            this.nudGridSize.Name = "nudGridSize";
            this.nudGridSize.Size = new System.Drawing.Size(63, 20);
            this.nudGridSize.TabIndex = 3;
            this.nudGridSize.Value = new decimal(new int[] { 1, 0, 0, 0 });
            this.nudGridSize.ValueChanged += new System.EventHandler(this.nudGridSize_ValueChanged);
            // 
            // lblGridMultiplier
            // 
            this.lblGridMultiplier.AutoSize = true;
            this.lblGridMultiplier.BackColor = System.Drawing.Color.Transparent;
            this.lblGridMultiplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGridMultiplier.Location = new System.Drawing.Point(78, 18);
            this.lblGridMultiplier.Name = "lblGridMultiplier";
            this.lblGridMultiplier.Size = new System.Drawing.Size(14, 15);
            this.lblGridMultiplier.TabIndex = 2;
            this.lblGridMultiplier.Text = "0";
            // 
            // btnSave
            // 
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(309, 240);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(152, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save changes and exit";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbPenTip1);
            this.groupBox2.Controls.Add(this.rbPenTip0);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(436, 48);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pen tip shape";
            // 
            // rbPenTip1
            // 
            this.rbPenTip1.AutoSize = true;
            this.rbPenTip1.Location = new System.Drawing.Point(107, 19);
            this.rbPenTip1.Name = "rbPenTip1";
            this.rbPenTip1.Size = new System.Drawing.Size(74, 17);
            this.rbPenTip1.TabIndex = 1;
            this.rbPenTip1.TabStop = true;
            this.rbPenTip1.Text = "Rectangle";
            this.rbPenTip1.UseVisualStyleBackColor = true;
            // 
            // rbPenTip0
            // 
            this.rbPenTip0.AutoSize = true;
            this.rbPenTip0.Location = new System.Drawing.Point(16, 19);
            this.rbPenTip0.Name = "rbPenTip0";
            this.rbPenTip0.Size = new System.Drawing.Size(63, 17);
            this.rbPenTip0.TabIndex = 0;
            this.rbPenTip0.TabStop = true;
            this.rbPenTip0.Text = "Elliptical";
            this.rbPenTip0.UseVisualStyleBackColor = true;
            // 
            // cbPressureSensitivity
            // 
            this.cbPressureSensitivity.AutoSize = true;
            this.cbPressureSensitivity.Location = new System.Drawing.Point(23, 22);
            this.cbPressureSensitivity.Name = "cbPressureSensitivity";
            this.cbPressureSensitivity.Size = new System.Drawing.Size(150, 17);
            this.cbPressureSensitivity.TabIndex = 0;
            this.cbPressureSensitivity.Text = "Enable pressure sensitivity";
            this.cbPressureSensitivity.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbLockCanvasToSquare);
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(6, 68);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(436, 40);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Shape";
            // 
            // cbLockCanvasToSquare
            // 
            this.cbLockCanvasToSquare.AutoSize = true;
            this.cbLockCanvasToSquare.Location = new System.Drawing.Point(9, 17);
            this.cbLockCanvasToSquare.Name = "cbLockCanvasToSquare";
            this.cbLockCanvasToSquare.Size = new System.Drawing.Size(176, 17);
            this.cbLockCanvasToSquare.TabIndex = 0;
            this.cbLockCanvasToSquare.Text = "Lock canvas to a square shape";
            this.cbLockCanvasToSquare.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(456, 222);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(448, 196);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Canvas settings";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(448, 196);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ink settings";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage3.Controls.Add(this.pbTablet);
            this.tabPage3.Controls.Add(this.cbPressureSensitivity);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(448, 196);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Drawing tablet settings";
            // 
            // pbTablet
            // 
            this.pbTablet.Image = ((System.Drawing.Image)(resources.GetObject("pbTablet.Image")));
            this.pbTablet.Location = new System.Drawing.Point(253, 3);
            this.pbTablet.Name = "pbTablet";
            this.pbTablet.Size = new System.Drawing.Size(192, 190);
            this.pbTablet.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbTablet.TabIndex = 1;
            this.pbTablet.TabStop = false;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage4.Controls.Add(this.lbCheckForUpdates);
            this.tabPage4.Controls.Add(this.pbLogo);
            this.tabPage4.Controls.Add(this.lblSubTitle);
            this.tabPage4.Controls.Add(this.lblKdTitle);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(448, 196);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "About";
            // 
            // lbCheckForUpdates
            // 
            this.lbCheckForUpdates.AutoSize = true;
            this.lbCheckForUpdates.LinkColor = System.Drawing.Color.Cyan;
            this.lbCheckForUpdates.Location = new System.Drawing.Point(20, 93);
            this.lbCheckForUpdates.Name = "lbCheckForUpdates";
            this.lbCheckForUpdates.Size = new System.Drawing.Size(94, 13);
            this.lbCheckForUpdates.TabIndex = 3;
            this.lbCheckForUpdates.TabStop = true;
            this.lbCheckForUpdates.Text = "Check for updates";
            this.lbCheckForUpdates.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbCheckForUpdates_LinkClicked);
            // 
            // pbLogo
            // 
            this.pbLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbLogo.Image")));
            this.pbLogo.InitialImage = null;
            this.pbLogo.Location = new System.Drawing.Point(303, 20);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(128, 128);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogo.TabIndex = 2;
            this.pbLogo.TabStop = false;
            // 
            // lblSubTitle
            // 
            this.lblSubTitle.AutoSize = true;
            this.lblSubTitle.Location = new System.Drawing.Point(94, 56);
            this.lblSubTitle.Name = "lblSubTitle";
            this.lblSubTitle.Size = new System.Drawing.Size(124, 13);
            this.lblSubTitle.TabIndex = 1;
            this.lblSubTitle.Text = "brought to you by eemhu";
            // 
            // lblKdTitle
            // 
            this.lblKdTitle.AutoSize = true;
            this.lblKdTitle.Font = new System.Drawing.Font("Rockwell", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKdTitle.Location = new System.Drawing.Point(17, 20);
            this.lblKdTitle.Name = "lblKdTitle";
            this.lblKdTitle.Size = new System.Drawing.Size(164, 36);
            this.lblKdTitle.TabIndex = 0;
            this.lblKdTitle.Text = "KanjiDraw";
            // 
            // advancedConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(476, 273);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnSave);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "advancedConfigForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KanjiDraw - Advanced settings";
            this.Load += new System.EventHandler(this.AdvancedConfigForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGridSize)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbTablet)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblGridMultiplier;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbPenTip1;
        private System.Windows.Forms.RadioButton rbPenTip0;
        private System.Windows.Forms.CheckBox cbPressureSensitivity;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox cbLockCanvasToSquare;
        private System.Windows.Forms.NumericUpDown nudGridSize;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Label lblSubTitle;
        private System.Windows.Forms.Label lblKdTitle;
        private System.Windows.Forms.LinkLabel lbCheckForUpdates;
        private System.Windows.Forms.PictureBox pbTablet;
    }
}
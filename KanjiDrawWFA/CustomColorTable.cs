﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KanjiDrawWFA
{
    public class CustomColorTable : ProfessionalColorTable
    {
        public CustomColorTable()
        {
            base.UseSystemColors = false;
        }

        private Color overrideColor = Color.FromArgb(200, 0, 0);

        public override Color MenuItemSelected
        {
            get { return overrideColor; }
        }

        public override Color MenuItemBorder
        {
            get { return overrideColor; }
        }

        public override Color MenuItemSelectedGradientBegin
        {
            get { return overrideColor; }
        }

        public override Color MenuItemSelectedGradientEnd
        {
            get { return overrideColor; }
        }

        public override Color MenuItemPressedGradientBegin
        {
            get { return overrideColor; }
        }

        public override Color MenuItemPressedGradientEnd
        {
            get { return overrideColor; }
        }

        public override Color MenuBorder
        {
            get { return overrideColor; }
        }

        public override Color MenuStripGradientBegin
        {
            get { return overrideColor; }
        }

        public override Color SeparatorDark
        {
            get { return overrideColor; }
        }

        public override Color SeparatorLight
        {
            get { return overrideColor; }
        }

        public override Color ToolStripPanelGradientBegin
        {
            get { return overrideColor; }
        }

        public override Color ButtonPressedBorder
        {
            get { return overrideColor; }
        }

        public override Color ButtonCheckedHighlight
        {
            get { return overrideColor; }
        }
    }
}

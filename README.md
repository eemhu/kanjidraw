# KanjiDraw

KanjiDraw is an application designed to aid the learners of Japanese (can be used for other languages and scripts as well)
in how to write the characters used in the writing system.

The application has been designed from the ground up to be as light as possible, while providing all the necessary features.

The author of this application (@eemhu) and some of his peers were dissatisfied by the options available for this purpose,
and general purpose drawing applications such as Paint or Photoshop seemed too resource intensive or otherwise didn't suit the needs.

That's where the story of KanjiDraw began.

***

## Features
* Change ink and background color of ink canvas
* Erase full strokes of ink (pixel-by-pixel erasing also available)
* Handy hotkeys to make the usage of the application easier
* Detailed adjustment of the pen tip size and shape
* Ability to view stroke order diagram (you no longer need to manually look it up, just draw on top!)
  -> This feature requires you to install the free font available from: [Nihilist.org.uk](https://www.nihilist.org.uk/)
* Ability to "rescue" drawings that you have accidentally cleared
* Customizable grid to help maintain the correct size of the character
* Settings will be automatically saved, so you can quickly continue from where you left off

## Technical details
KanjiDraw is completely written in C# and is available for Windows.
It does not require any installation, the software is fully portable (outside of settings, as they are saved to the user folder)

## Screenshot
![Screenshot of the UI](https://i.imgur.com/V3TBJ0x.png)